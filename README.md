# Dynamic library in Linux #

This is a sample project using a dynamic library in Linux.
  
## Description  ##

The main executable dynamically loads a library and call a function to instantiate
an Calc object. Then call a method through the ICalc interface.


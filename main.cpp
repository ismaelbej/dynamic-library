#include "ICalc.h"
#include <iostream>
#include <dlfcn.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

int main()
{
    char libdir[PATH_MAX];
    if (!getcwd(libdir, PATH_MAX)) {
        std::cerr << "Failed to get current directory" << std::endl;
        exit(-1);
    }

    strcat(libdir, "/libcalc.so");

    void* lib;
    lib = dlopen(libdir, RTLD_LAZY);
    if (!lib) {
        std::cerr << "Failed to load libcalc.so" << std::endl;
        exit(-1);
    }

    typedef ICalc* (*CreateCalcType)();
    CreateCalcType pCreateCalc = nullptr;

    pCreateCalc = (CreateCalcType)dlsym(lib, "createCalc");
    if (dlerror() != NULL) {
        std::cerr << "Failed to get symbol" << std::endl;
        dlclose(lib);
        exit(-1);
    }

    ICalc* pCalc = (*pCreateCalc)();
    if (!pCalc) {
        std::cerr << "Failed to create calc" << std::endl;
        dlclose(lib);
        exit(-1);
    }

    int res = pCalc->add(1, 2);

    std::cout << "Result: 1 + 2 = " << res << std::endl;

    delete pCalc;
    dlclose(lib);

    return 0; 
}


CXX := c++
LD := c++
LDFLAGS := -std=c++11
LOBJECTS := Calc.o
OBJECTS := main.o
CXXFLAGS := -std=c++11 -g -I. -fPIC

.PHONY: run clean

main : $(OBJECTS)
	$(LD) -o $@ $^ $(LDFLAGS) -ldl

%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

run : main libcalc.so
	./main

clean:
	-$(RM) $(OBJECTS)
	-$(RM) $(LOBJECTS)

libcalc.so : $(LOBJECTS)
	$(CC) -shared -o $@ $^


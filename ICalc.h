#pragma once

class ICalc;

extern "C" {
    ICalc* createCalc();
}

class ICalc
{
    public:
        virtual ~ICalc() {}

        virtual int add(int a, int b) const = 0;
        virtual int mul(int a, int b) const = 0;
};


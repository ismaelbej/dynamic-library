#pragma once
#include "ICalc.h"

class Calc : public ICalc
{
    public:
        Calc();
        virtual ~Calc();

        virtual int add(int a, int b) const;
        virtual int mul(int a, int b) const;
};


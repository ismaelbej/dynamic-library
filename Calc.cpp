#include "Calc.h"

ICalc* createCalc()
{
    return new Calc();
}


Calc::Calc()
{
}

Calc::~Calc()
{
}

int Calc::add(int a, int b) const
{
    return a + b;
}

int Calc::mul(int a, int b) const
{
    return a * b;
}

